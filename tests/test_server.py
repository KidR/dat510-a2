from http import HTTPStatus
from server import UserRegistry
import requests

HEADERS={"Content-Type": "application/json"}

class TestServer:

    @classmethod
    def setup_class(cls):
        registry = UserRegistry('registry.json')
        cls.urls = {}
        cls.users = []
        print("DEBUG",registry)
        for username, user in registry.registry.items():
            cls.urls[username] = user['url']
            cls.users.append(username)
        if len(cls.users) < 2:
            raise ValueError("***** Mandatory to have at least 2 users in registry.")

    @classmethod
    def teardown_class(cls):
        pass

    def test_key_exchange(self):
        user_0 = self.users[0]
        user_1 = self.users[1]
        response = requests.get(f'{self.urls[user_0]}/key_exchange/simulate?user={user_1}',headers=HEADERS)
        assert response.json()["status"] == HTTPStatus.ACCEPTED
        assert user_1 in response.json()["info"]

    def test_send_message(self):
        user_0 = self.users[0]
        user_1 = self.users[1]
        message = "She like the way that I dance, she like the way that I move."
        response = requests.get(f'{self.urls[user_0]}/messages/simulate?user={user_1}&message={message}',headers=HEADERS)
        assert response.json()["status"] == HTTPStatus.OK
        assert response.json()["to"] == user_1
        assert "message" in response.json()

    def test_read_message(self):
        user_0 = self.users[0]
        user_1 = self.users[1]
        response = requests.get(f'{self.urls[user_1]}/messages/{user_0}',headers=HEADERS)
        assert response.json()["status"] == HTTPStatus.OK
        assert response.json()["from"] == user_0
        assert "message" in response.json() and "encrypted" in response.json() 
