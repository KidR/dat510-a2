from typing import Union
from .csprng import BBS, CSPRNG
from .sdes import SymmetricCipher, TripleSDES
from .dh import DH
import abc

class UnknownUserError(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)
class User(abc.ABC):

    def __init__(self, name: str, q: int = None) -> None:
        """Initialize User.

        Args:
            name (str): username
        """
        self.name = name
        self._exchanged_keys = {}
        self._csprng = None
        self._symmetric_cipher = None
        self._sent = {}
        self._received = {}
        self._dh = DH(q)

    @abc.abstractmethod
    def send_message_to(self, receiver: object, message: str) -> bytes:
        """Send message to {receiver}.

        Args:
            receiver (str): receiver name
            message (str): message to send

        Returns:
            bytes: message stream bytes
        """
        pass

    @abc.abstractmethod
    def read_message_from(self, sender: object) -> bytes:
        """Read message from {receiver}.

        Args:
            sender (User): sender name

        Returns:
            bytes: message stream bytes
        """
        pass

    @abc.abstractmethod
    def exchange_key(self, user: object):
        """Generate key exchange from a given public key.

        Args:
            public_key (User3SDES): public key from a user itself

        Returns:
            (int): key exchange
        """
        pass

    @abc.abstractmethod
    def get_message_from(self, sender: Union[object, str]) -> bytes:
        """[summary]

        Args:
            sender (Union[User,str]): sender of message

        Returns:
            bytes: message
        """
        pass

    @abc.abstractmethod
    def get_exchanged_key(self,user: object) -> int:
        """Get key exchanged with {user}.

        Args:
            user (object | str): user or username

        Returns:
            int: key exchanged with {user}
        """
        pass

    @abc.abstractproperty
    def public_key(self) -> int:
        """Compute the public key

        Returns:
            int: public key
        """
        pass

    @property
    def csprng(self) -> CSPRNG:
        """Compute CSPRNG class chosen.

        Returns:
            str: csprng class name
        """
        return self._csprng.__name__

    @csprng.setter
    def csprng(self, csprng: CSPRNG):
        """Set CSPRNG class chosen.

        Args:
            csprng (CSPRNG): CSPRNG class

        Raises:
            TypeError: Must be CSPRNG type
        """
        if not issubclass(csprng, CSPRNG):
            raise TypeError("***** Must be CSPRNG type.")
        self._csprng = csprng

    @property
    def symmetric_cipher(self) -> SymmetricCipher:
        """Compute SymmetricCipher class chosen.

        Returns:
            str: SymmetricCipher class name
        """
        return self._symmetric_cipher.__name__

    @symmetric_cipher.setter
    def symmetric_cipher(self, symmetric_cipher: SymmetricCipher):
        """Set SymmetricCipher class chosen.

        Args:
            symmetric_cipher (SymmetricCipher): SymmetricCipher class
        Raises:
            TypeError:  Must be SymmetricCipher type
        """
        if not issubclass(symmetric_cipher, SymmetricCipher):
            raise TypeError("***** Must be SymmetricCipher type.")
        self._symmetric_cipher = symmetric_cipher

    @property
    def q(self) -> int:
        """Compute prime number q, the global parameter of Diffie-Hellman.

        Returns:
            int: prime number q
        """
        return self._dh.q

    @property
    def a(self) -> int:
        """Compute primitive root a (generator), the global parameter of Diffie-Hellman.

        Returns:
            int: primitive root a
        """
        return self._dh.a
    
    @property
    def sent(self) -> dict:
        """Compute message sent to all users.

        Returns:
            dict: message sent
        """
        return self._sent
    
    @property
    def received(self) -> dict:
        """Compute message received from all users.

        Returns:
            dict: message received
        """
        return self._received

    def __str__(self):
        """Represents User characteristics.

        Returns:
            (str): user information
        """
        return f"""{self.name}:\texchanged_key={self._exchanged_keys}"""


class User3SDES(User,abc.ABC):
    def __init__(self, name: str, q: int = None):
        """Initialize User of Diffie-Hellman algorithm.

        Args:
            name (str): username
            dh (DH): Diffie-Hellman instance
        """
        super().__init__(name, q)
        self.symmetric_cipher = TripleSDES

    def generate_message_key(self, seed: int) -> bytes:
        """Generate secrete key for sending and read message.

        Args:
            seed (int): exchange seed
        """
        return self._csprng(seed=seed).generate_secret_key(flag=self._symmetric_cipher.__name__)

    def send_message_to(self, receiver: object, message: str) -> bytes:
        """Send message to {receiver}.

        Args:
            receiver (object): receiver name
            message (str): message to send

        Returns:
            bytes: message stream bytes
        """
        return self.generate_message_key(self.get_exchanged_key(receiver))
        

    def read_message_from(self, sender: User) -> bytes:
        """Read message from {receiver}.

        Args:
            sender (User): sender name

        Returns:
            bytes: message stream bytes
        """
        return self.generate_message_key(self.get_exchanged_key(sender))

    def get_exchanged_key(self,user: object) -> int:
        """Get key exchanged with {user}.

        Args:
            user (str): username

        Returns:
            int: key exchanged with {user}
        """
        if not isinstance(user,str):
            raise TypeError(f"***** User must be str.")
        key = self._exchanged_keys.get(user)
        if not key:
            raise UnknownUserError(f"***** User '{self.name}' must exchange key with user '{user}' first.")
        return key

    def get_message_from(self, sender: object) -> bytes:
        """Get encrypted message from {sender}.

        Args:
            sender (object): sender of message

        Returns:
            bytes: message
        """
        if not isinstance(sender, str):
            raise TypeError("***** type must be str.")
        return self._received[sender]

    def __str__(self):
        """Represents User3SDES characteristics.

        Returns:
            (str): user information
        """
        return f"""{super().__str__()}\tpublic_key={self.public_key}"""

class LocalUser3SDES(User3SDES):

    def __init__(self, name: str, q: int = None):
        super().__init__(name, q=q)
        self.__keys = self._dh.generate_keygen()

    @property
    def public_key(self) -> int:
        """Compute the public key

        Returns:
            int: public key
        """
        return self.__keys[self._dh.PUBLIC_KEY]

    def exchange_key(self, user: object):
        """Generate key exchange from a given public key.

        Args:
            public_key (User3SDES): public key from a user itself

        Returns:
            (int): key exchange
        """
        if not isinstance(user, User3SDES):
            raise TypeError(
                "***** Public key should be integer value or User3SDES instance."
            )
        self._exchanged_keys[user.name] = (user.public_key ** self.__keys[DH.PRIVATE_KEY]) % self.q
        return self._exchanged_keys[user.name]

    def send_message_to(self, receiver: User, message: str) -> bytes:
        """Send message to {receiver}.

        Args:
            receiver (User): receiver name
            message (str): message to send

        Returns:
            bytes: message stream bytes
        """
        secret_keys = super().send_message_to(receiver,message).decode()
        self._sent[receiver.name] = self._symmetric_cipher.encrypt_from_bytes(
            secret_keys[:10], secret_keys[10:], message.encode()
        )
        receiver._received[self.name] = self._sent[receiver.name]
        return self._sent[receiver.name]

    def read_message_from(self, sender: User) -> bytes:
        """Read message from {receiver}.

        Args:
            sender (User): sender name

        Returns:
            bytes: message stream bytes
        """
        secret_keys = super().read_message_from(sender).decode()
        self._received[sender.name] = self._symmetric_cipher.decrypt_from_bytes(
            secret_keys[:10], secret_keys[10:], self._received[sender.name]
        )
        return self._received[sender.name]

    def get_exchanged_key(self,user: object) -> int:
        """Get key exchanged with {user}.

        Args:
            user (User): user

        Returns:
            int: key exchanged with {user}
        """
        return super().get_exchanged_key(user.name)

    def get_message_from(self, sender: object) -> bytes:
        """Get encrypted message from {sender}.

        Args:
            sender (object): sender of message

        Returns:
            bytes: message
        """
        return super().get_message_from(sender.name)