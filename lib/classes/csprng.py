import abc
import sympy as sp
from typing import Dict
from .utils import PrimeNumberUtils
from .sdes import SDES, TripleSDES
from random import randint, Random


class CSPRNG(abc.ABC):
    @abc.abstractmethod
    def __init__(self, seed: any):
        """Initialize CSPRNG.

        Args:
            seed (any): seed of generator
        """
        pass

    @abc.abstractmethod
    def generate_secret_key(
        self, bits: int = 256, flag: str = None, encoding: str = "utf-8"
    ) -> bytes:
        """Generate a secret key according by BlumBlumShub algorithm.

        Args:
            bits (int): bits length of key

        Returns:
            bytes: generated bits as key
        """
        pass


class BBS(CSPRNG):

    PRIME_LOWER_BOUND: int = 10 ** 2  # 10**5

    PRIME_UPPER_BOUND: int = 10 ** 4  # 10**8

    FLAG_AES_128: str = "AES-128"
    FLAG_AES_192: str = "AES-192"
    FLAG_AES_256: str = "AES-256"
    FLAG_SDES: str = SDES.__name__
    FLAG_3SDES: str = TripleSDES.__name__

    AUTHORIZED_BITS: Dict[str, int] = {
        FLAG_AES_128: 128,
        FLAG_AES_192: 192,
        FLAG_AES_256: 256,
        FLAG_SDES: 10,
        FLAG_3SDES: 20,
    }

    def __init__(self, seed: any = None):
        """Initialize BBS generator.

        Args:
            seed (any, optional): seed. Defaults to None.
        """
        super().__init__(seed)
        if seed:
            self.__seed = seed
        else:
            self.__seed = randint(2, self.__n)
            while not PrimeNumberUtils.is_relative_prime(self.__seed, self.__n):
                self.__seed = randint(2, self.__n)
        pseudo_random = Random(self.__seed)
        respect_modulo_requirement = False
        while not respect_modulo_requirement:
            _p = pseudo_random.randint(self.PRIME_LOWER_BOUND, self.PRIME_UPPER_BOUND)
            _q = pseudo_random.randint(self.PRIME_LOWER_BOUND, self.PRIME_UPPER_BOUND)
            self.__p = sp.prime(_p)
            self.__q = sp.prime(_q)
            respect_modulo_requirement = self.__check_modulo_requirement(
                self.__p, self.__q
            )
        self.__n = self.__p * self.__q

    def __check_modulo_requirement(self, p: int, q: int) -> bool:
        """Check the following relation p = q = 3 mod 4.

        Args:
            p (int): large prime number p
            q (int): large prime number q

        Returns:
            bool: True if relation is correct else False
        """
        return (p % 4) == 3 and (q % 4) == 3

    def generate_secret_key(
        self, bits: int = 256, flag: str = None, encoding: str = "utf-8"
    ) -> bytes:
        """Generate a secret key according by BlumBlumShub algorithm.

        Args:
            bits (int): bits length of key

        Returns:
            bytes: generated bits as key
        """
        super().generate_secret_key()
        f_x = lambda x: (x ** 2) % self.__n
        x, b = [], ""
        x0 = f_x(self.__seed)
        for i in range(self.AUTHORIZED_BITS.get(flag, bits)):
            x.append(f_x(x0))
            b += str(x[i] % 2)
            x0 = x[i]
        return b.encode(encoding)

    def debug(self):
        print(f"DEBUG: p={self.__p}\tq={self.__q}\tn={self.__n}\ts={self.__seed}")
