from random import randint
import sympy as sp


class DH:
    """Diffie-Hellman class."""

    PRIME_LOWER_BOUND = 10 ** 2 # 10**5
    PRIME_UPPER_BOUND = 10 ** 4  # 10**8
    PRIME_UPPER_BOUND_BIS = 10 ** 5

    PUBLIC_KEY = "public_key"
    PRIVATE_KEY = "private_key"

    def __init__(self, q: int = None) -> None:
        """Initialize global parameters to Diffie-Hellman.

        Args:
            q (int, optional): prime number. Defaults to 29.
            a (int, optional): primitive root of q. Defaults to 2.
        """
        self.__q = (
            q if q else sp.randprime(self.PRIME_LOWER_BOUND, self.PRIME_UPPER_BOUND)
        )

    @property
    def q(self) -> int:
        """Return global parameter q which is prime number."""
        return self.__q

    @property
    def p(self) -> int:
        """Compute prime number p such as p = 2*q + 1.

        Args:
            q (int): prime number

        Returns:
            int: new prime number p

        Raises:
            ValueError: q is not a prime number
            ValueError: p computed by q is not a prime number
        """
        if not sp.isprime(self.q):
            raise ValueError(f"***** q value '{self.q}' must be a prime number.")
        p = sp.randprime(self.q, self.PRIME_UPPER_BOUND_BIS)
        if not sp.isprime(p) and p > self.q:
            raise ValueError(
                f"***** q value '{self.q}' must be changed in order to have a prime number p '{p}'"
            )
        return p

    @property
    def a(self) -> int:
        """Return global parameter a which is primitive root of q."""
        return sp.primitive_root(self.q)

    def generate_keygen(self) -> dict:
        """Generate private and public key by applying Diffie-Hellman.

        Returns:
            (dict): public and private key
        """
        x = randint(0, self.q - 1)
        y = (self.a ** x) % self.q
        return {self.PUBLIC_KEY: y, self.PRIVATE_KEY: x}
