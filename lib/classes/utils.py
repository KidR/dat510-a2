import sympy as sp


class DomainError(Exception):
    """Domain error to restraint numbers."""

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class PrimeNumberUtils:

    """domain defined on positive integers N."""

    domain: bool = lambda n: n >= 0 and isinstance(n, int)

    @classmethod
    def is_relative_prime(cls, a: int, b: int) -> bool:
        """Check if two integers are relatively prime to each other.

        Args:
            a (int): number
            b (int): number

        Returns:
            bool: True if gcd(a,b) == 1 else False
        """
        if not cls.domain(a) or not cls.domain(b):
            raise DomainError("***** values have to be zeros or positives integers.")
        return sp.igcd(a, b) == 1
