"""Module for SDES and TripleSDES cipher."""
from typing import Tuple, Union
import abc


class SymmetricCipher(abc.ABC):

    pass


class Utils:
    """Utils methods used by module classes."""

    DEFAULT_ENCODING = "utf-8"

    @classmethod
    def binary(cls, n: int, size: int, encoding: str = None) -> Union[str, bytes]:
        """
        Build a binary size-block of the integer n.

        Args:
            n (int): number to transform in binary
            size (int): binary size block
            encoding (str): encoding

        Returns:
            Union[str,bytes]: binary block
        """
        binary = ("{:0>%db}" % size).format(n)
        return binary.encode() if encoding else binary

    @classmethod
    def binary_to_char(cls, binary: str) -> str:
        """
        Convert a binary block into a char.

        Args:
            binary (str): binary string

        Returns:
            str: char according to the UNICODE table
        """
        return chr(int(binary, 2))

    @classmethod
    def binary_to_bytes(cls, input: str, encoding: str = DEFAULT_ENCODING) -> bytes:
        """
        Convert input binary to bytes.

        Args:
            input (str): input
            encoding (str): encoding
        Returns:
            bytes: bytes value
        """
        return bytes.fromhex(f"{cls.binary_to_hex(input)}").decode(encoding)

    @classmethod
    def binary_to_hex(cls, input: str) -> str:
        """Convert input binary to hex.

        Args:
            input (str): input
            encoding (str): encoding
        Returns:
            str: hex value
        """
        return f"{int(input,2):0>2X}"

    @classmethod
    def bytes_to_int(cls, input: bytes) -> int:
        """
        Convert input bytes to int.

        Args:
            input (bytes): input

        Returns:
            (int): int value
        """
        return int(input.hex(), 16)

    @classmethod
    def bytes_to_binary(cls, input: bytes, encoding: str = None) -> Union[str, bytes]:
        """
        Convert input bytes to binary.

        Args:
            input (bytes): input
            encoding (str): encoding

        Returns:
            Union[str,bytes]: binary value
        """
        return cls.binary(cls.bytes_to_int(input), size=8, encoding=encoding)

    @classmethod
    def hex_to_binary(cls, input: str, size: int) -> str:
        """Convert hex to binary.

        Args:
            input (str): hex value
            size (int): binary block size

        Returns:
            str: binary
        """
        return ("{:0>%db}" % size).format(int(input, 16))

    @classmethod
    def xor(cls, b1: str, b2: str) -> str:
        """
        Apply the logicial XOR on two block of bits of the same length.

        Args:
            b1 (str): 1st block of bits
            b2 (str): 2nd block of bits

        Returns:
            str: new block of bits resulted by the XOR

        Raises:
            ValueError: blocks of bits must be from the same length.
        """
        if len(b1) != len(b2):
            raise ValueError("***** Blocks of bits must be from the same length.")
        return "".join([str(x ^ y) for x, y in zip(b1, b2)])


class Keygen:
    """Key generation class for SDES and Triple SDES."""

    @classmethod
    def p10(cls, bits: str) -> str:
        """
        Apply the permutation P10.

        Args:
            bits (str): block of 10-bits

        Returns:
            str: new block of 10-bits permuted
        """
        return (
            bits[2]
            + bits[4]
            + bits[1]
            + bits[6]
            + bits[3]
            + bits[9]
            + bits[0]
            + bits[8]
            + bits[7]
            + bits[5]
        )

    @classmethod
    def ls1(cls, bits: str) -> str:
        """
        Apply the left-shift with a step of 1 on the first 5-bits block and on the second 5-bits block.

        Args:
            bits (str): block of 10-bits

        Returns:
            (str): new block of 10-bits left-shifted
        """
        half1 = bits[:5]
        half2 = bits[5:]
        leftshift = lambda s, n: s[n:] + s[:n]
        return leftshift(half1, 1) + leftshift(half2, 1)

    @classmethod
    def p8(cls, bits: str) -> str:
        """
        Apply the permutation P8.

        Args:
            bits (str): block of 10-bits

        Returns:
            str: new block of 8-bits permuted
        """
        return (
            bits[5]
            + bits[2]
            + bits[6]
            + bits[3]
            + bits[7]
            + bits[4]
            + bits[9]
            + bits[8]
        )

    @classmethod
    def ls2(cls, bits: str) -> str:
        """
        Apply the left-shift with a step of 2 on the first 5-bits block and on the second 5-bits block.

        Args:
            bits (str): block of 10-bits

        Returns:
            (str): new block of 10-bits left-shifted twice
        """
        return cls.ls1(cls.ls1(bits))

    @classmethod
    def sdes_keys(cls, bits: str) -> Tuple[str, str]:
        """
        Generate the SDES keys.

        Args:
            bits (str): block of 10-bits

        Returns:
            (tuple) generated keys k1 and k2 with length of 10.
        """
        x1 = cls.ls1(cls.p10(bits))
        k1 = cls.p8(x1)
        k2 = cls.p8(cls.ls2(x1))
        return k1, k2


class SDES(SymmetricCipher):
    """Simplified DES class."""

    KEY_LENGTH = 10
    TEXT_BLOCK_LENGHT = 8

    @classmethod
    def ip(cls, bits: str) -> str:
        """
        Apply the IP function on block of 8-bits.

        Args:
            bits (str): block of 8-bits

        Returns:
            str: block of 8-bits permuted by IP
        """
        if len(bits) != cls.TEXT_BLOCK_LENGHT:
            raise ValueError
        return (
            bits[1]
            + bits[5]
            + bits[2]
            + bits[0]
            + bits[3]
            + bits[7]
            + bits[4]
            + bits[6]
        )

    @classmethod
    def inv_ip(cls, bits: str) -> str:
        """
        Apply the inverse of IP function on block of 8-bits.

        Args:
            bits (str): block of 8-bits

        Returns:
            str: block of 8-bits permuted by the inverse of IP
        """
        if len(bits) != cls.TEXT_BLOCK_LENGHT:
            raise ValueError
        return (
            bits[3]
            + bits[0]
            + bits[2]
            + bits[4]
            + bits[6]
            + bits[1]
            + bits[7]
            + bits[5]
        )

    @classmethod
    def fk(cls, key: str, lbits: str, rbits: str) -> Tuple[str, str]:
        """
        Apply the fk function with the given subkey on left-bits and right-bits.

        Args:
            key (str): subkey
            lbits (str): left block of 4-bits
            rbits (str): right block of 4-bits

        Raises:
            ValueError: left-bits and right-bits must have the same length.

        Returns:
            Tuple[str,str]: left-bits encoded by fmapping function and initial right-bits
        """
        if len(lbits) != 4 and len(rbits) != 4:
            raise ValueError(
                "***** left-bits and right-bits must have the same length."
            )
        fmapping = cls.fmapping(rbits, key)
        return Utils.xor(lbits.encode(), fmapping.encode()), rbits

    @classmethod
    def fmapping(cls, bits: str, subkey: str) -> str:
        """
        Apply the Fmapping on a block of 8-bits.

        Args:
            bits (str): block of 4-bits
            subkey (str): block of 8-bits

        Returns:
            str: block of 8-bits mapped
        """

        def expansion(bits: str):
            return (
                bits[3]
                + bits[0]
                + bits[1]
                + bits[2]
                + bits[1]
                + bits[2]
                + bits[3]
                + bits[0]
            )

        def sbox(box):
            return [list(map(lambda b: Utils.binary(b, 2), row)) for row in box]

        def process_sbox(box, bits):
            if len(bits) != 4:
                raise ValueError
            row = int(bits[0] + bits[3], 2)
            col = int(bits[1] + bits[2], 2)
            return box[row][col]

        def p4(bits):
            if len(bits) != 4:
                raise ValueError
            return bits[1] + bits[3] + bits[2] + bits[0]

        if len(bits) != 4 and len(subkey) != 8:
            raise ValueError

        BOX_0 = [[1, 0, 3, 2], [3, 2, 1, 0], [0, 2, 1, 3], [3, 1, 3, 2]]

        BOX_1 = [[0, 1, 2, 3], [2, 0, 1, 3], [3, 0, 1, 0], [2, 1, 0, 3]]

        pbits = Utils.xor(expansion(bits).encode(), subkey.encode())
        return p4(
            process_sbox(sbox(BOX_0), pbits[:4]) + process_sbox(sbox(BOX_1), pbits[4:])
        )

    @classmethod
    def sw(cls, bits: str) -> str:
        """
        Apply switch function.

        Args:
            bits (str): block of 8-bits

        Returns:
            str: block of 8-bits switched
        """
        return bits[4:] + bits[:4]

    @classmethod
    def encrypt(cls, key: str, bits: str) -> str:
        """
        Encrypt the block of bits with the SDES algorithm with the given key.

        Args:
            key (str): block of 10-bits
            bits (str): block of 8-bits to cipher

        Returns:
            str: block of bits ciphered
        """
        keys = Keygen.sdes_keys(key)
        ip = cls.ip(bits)
        fk1 = cls.fk(keys[0], ip[:4], ip[4:])
        sw = cls.sw(fk1[0] + fk1[1])
        fk2 = cls.fk(keys[1], sw[:4], sw[4:])
        return cls.inv_ip(fk2[0] + fk2[1])

    @classmethod
    def decrypt(cls, key: str, bits: str) -> str:
        """
        Decrypt the block of bits with the SDES algorithm with the given key.

        Args:
            key (str): block of 10-bits
            bits (str): block of 8-bits to decipher

        Raises:
            ValueError: key length must be 10.
            ValueError: bits should be a block multiple of 8-bits.

        Returns:
            str: blocks of bits deciphered
        """
        if len(key) != 10:
            raise ValueError("***** key length must be 10.")
        if len(bits) % cls.TEXT_BLOCK_LENGHT != 0:
            raise ValueError("***** bits should be a block multiple of 8-bits.")

        def f_decrypt(block: str) -> str:
            keys = Keygen.sdes_keys(key)
            ip = cls.ip(block)
            fk2 = cls.fk(keys[1], ip[:4], ip[4:])
            sw = cls.sw(fk2[0] + fk2[1])
            fk1 = cls.fk(keys[0], sw[:4], sw[4:])
            return cls.inv_ip(fk1[0] + fk1[1])

        return f_decrypt(bits)

    @classmethod
    def encrypt_from_bytes(
        cls, key: str, input: bytes, encoding: str = "utf-8"
    ) -> bytes:
        """Encrypt the string with the SDES algorithm with the given key.

        Args:
            key (str): block of 10-bits
            input (bytes): text
            encoding (str): encoding

        Returns:
            bytes: ciphertext bytes
        """
        result = ""
        block_bytes = Utils.bytes_to_binary(input)
        if len(block_bytes) % 2 == 1:
            block_bytes = "0" + block_bytes
        for i in range(len(block_bytes) // cls.TEXT_BLOCK_LENGHT):
            cipher_block = cls.encrypt(
                key,
                block_bytes[
                    i * cls.TEXT_BLOCK_LENGHT : (i + 1) * cls.TEXT_BLOCK_LENGHT
                ],
            )
            result += Utils.binary_to_hex(cipher_block)
        return result.encode(encoding)

    @classmethod
    def decrypt_from_bytes(
        cls, key: str, input: bytes, encoding: str = "utf-8"
    ) -> bytes:
        """Encrypt the string with the SDES algorithm with the given key.

        Args:
            key (str): block of 10-bits
            input (bytes): cipher input
            encoding (str): encoding

        Returns:
            (bytes): deciphered bytes
        """
        result = ""
        for i in range(len(input) // 2):
            b = input[i * 2 : (i + 1) * 2]
            decipher_block = cls.decrypt(key, Utils.hex_to_binary(b, 8))
            result += decipher_block
        return Utils.binary_to_bytes(result).encode(encoding)


class TripleSDES(SymmetricCipher):
    """Triple Simplified DES class."""

    KEY_LENGTH = 10
    TEXT_BLOCK_LENGHT = 8

    def __init__(self) -> None:
        super().__init__()

    @classmethod
    def encrypt(cls, k1: str, k2: str, bits: str) -> str:
        """
        Encrypt the block of bits with the Triple SDES algorithm with the given key.

        Args:
            k1 (str): block of 10-bits as first key
            k2 (str): block of 10-bits as second key
            bits (str): block of 8-bits to cipher

        Returns:
            str: block of bits ciphered
        """
        return SDES.encrypt(k1, SDES.decrypt(k2, SDES.encrypt(k1, bits)))

    @classmethod
    def decrypt(cls, k1: str, k2: str, bits: str) -> str:
        """
        Decrypt the block of bits with the Triple SDES algorithm with the given key.

        Args:
            k1 (str): block of 10-bits as first key
            k2 (str): block of 10-bits as second key
            bits (str): block of 8-bits to decipher

        Returns:
            str: block of bits deciphered
        """
        return SDES.decrypt(k1, SDES.encrypt(k2, SDES.decrypt(k1, bits)))

    @classmethod
    def encrypt_from_bytes(
        cls, k1: str, k2: str, input: bytes, encoding: str = "utf-8"
    ) -> bytes:
        """Encrypt the string with the 3SDES algorithm with the given key.

        Args:
            k1 (str): block of 10-bits as key 1
            k2 (str): block of 10-bits as key 2
            input (bytes): text
            encoding (str): encoding

        Returns:
            bytes: ciphertext bytes
        """
        result = ""
        block_bytes = Utils.bytes_to_binary(input)
        if len(block_bytes) % 2 == 1:
            block_bytes = "0" + block_bytes
        for i in range(len(block_bytes) // cls.TEXT_BLOCK_LENGHT):
            cipher_block = cls.encrypt(
                k1,
                k2,
                block_bytes[
                    i * cls.TEXT_BLOCK_LENGHT : (i + 1) * cls.TEXT_BLOCK_LENGHT
                ],
            )
            result += Utils.binary_to_hex(cipher_block)
        return result.encode(encoding)

    @classmethod
    def decrypt_from_bytes(
        cls, k1: str, k2: str, input: bytes, encoding: str = "utf-8"
    ) -> bytes:
        """Encrypt the string with the 3SDES algorithm with the given key.

        Args:
            k1 (str): block of 10-bits as key 1
            k2 (str): block of 10-bits as key 2
            input (bytes): cipher input
            encoding (str): encoding

        Returns:
            (bytes): deciphered bytes
        """
        result = ""
        for i in range(len(input) // 2):
            b = input[i * 2 : (i + 1) * 2]
            decipher_block = cls.decrypt(k1, k2, Utils.hex_to_binary(b, 8))
            result += decipher_block
        return Utils.binary_to_bytes(result).encode(encoding)
