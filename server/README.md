# Server

## Routes

TODO: update the doc.

`/messages/<username>`
- GET: read encrypted and decrypted message from `<username>`
- POST: send message to `<username>`
    - data: {`<message>`}

`/key_exchange`
- GET ?user: exchange key with `<user>` by looking to registry
- POST: exchange key 
    - data: {`<public_key>`, `<username>`}
    - response: {`<public_key>`, `<username>`, `<exchange_key>`}