from pathlib import Path
from filelock import FileLock

import json 

class UserRegistry:

    def __init__(self,filename: str):
        """Lookup the registry

        Args:
            filename (str): registry filename
        """
        self.path = Path(filename)
        self.registry = {}
        with FileLock(f"{self.path.name}.lock") as fl:
            if not self.path.exists():
                self.commit()
            else:
                self.load_registry(self.path.name)
            fl.release(True)

    def load_registry(self,filename: str):
        """Load registry.

        Args:
            filename (str): registry to load
        """
        with open(filename,"r") as fp:
            self.registry = json.load(fp)

    def find_user(self, user: str) -> dict:
        """Returns data for a given {user}.

        Args:
            user (str): username

        Returns:
            dict: data of {user}
        """
        return self.registry.get(user)

    def add_user(self, user: str, **data):
        """Add user and its data in the registry.

        Args:
            user (str): username
            **data (dict): data of user
        """
        self.registry[user] = data

    def commit(self) -> int:
        """Commit changes in registry.

        Returns:
            int: write output
        """
        with open(self.path.name,"w") as fp:
            fp.write(self.__str__())
            

    def __str__(self) -> str:
        return json.dumps(self.registry,indent=2)

    def __repr__(self):
        for user, data in self.registry.items():
            print(f"{user}\t{json.dumps(data,indent=2)}")
        return

    def __len__(self) -> int:
        return self.registry.__len__()