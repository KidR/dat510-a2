from lib import User3SDES, DH
import requests
import json

class WebUser3SDES(User3SDES):

    def __init__(self, name: str, url: str, q: int = None) -> None:
        """Initialize web User of Diffie-Hellman algorithm.

        Args:
            name (str): username
            dh (DH): Diffie-Hellman instance
        """
        super().__init__(name, q=q)
        self.__keys = self._dh.generate_keygen()
        self.url = url

    @property
    def public_key(self) -> int:
        """Compute the public key

        Returns:
            int: public key
        """
        return self.__keys[self._dh.PUBLIC_KEY]

    def exchange_key(self, user: dict) -> int:
        """Generate key exchange by using user payload.

        Args:
            user (dict): user payload

        Returns:
            int: generated key

        Payload:
            - "public_key": public key of the users
            - "name": name of the user
        """
        if not isinstance(user["public_key"], int):
            raise TypeError("***** user must be str and public key must be int.")
        self._exchanged_keys[user["username"]] = (user["public_key"] ** self.__keys[DH.PRIVATE_KEY]) % self.q
        return self._exchanged_keys[user["username"]]

    
    def send_message_to(self, receiver: str, message: str) -> bytes:
        """Send message to {receiver}.

        Args:
            receiver (str): receiver url
            message (str): message to send

        Returns:
            bytes: message stream bytes
        """
        secret_keys = super().send_message_to(receiver,message).decode()
        self._sent[receiver] = self._symmetric_cipher.encrypt_from_bytes(
            secret_keys[:10], secret_keys[10:], message.encode()
        )
        return self._sent[receiver]

    
    def read_message_from(self, sender: str) -> bytes:
        """Read message from {receiver}.

        Args:
            sender (str): sender name

        Returns:
            bytes: message stream bytes
        """
        secret_keys = super().read_message_from(sender).decode()
        self._received[sender] = self._symmetric_cipher.decrypt_from_bytes(
            secret_keys[:10], secret_keys[10:], self._received[sender]
        )
        return self._received[sender]

    def get_message_from(self, sender: str) -> bytes:
        """Get encrypted message from {sender}.

        Args:
            sender (str): sender of message

        Returns:
            bytes: message
        """
        return super().get_message_from(sender)

    def receive_message_from(self, sender: str, message: str):
        """Receive message from {sender}.

        Args:
            sender (str): sender name
        """
        self._received[sender] = message

    def build_payload(self,**payload):
        return json.dumps(payload)