from flask import Flask, request, url_for
from http import HTTPStatus

from flask.templating import render_template

from lib import BBS
from server import WebUser3SDES


import requests
import sys
import json
import configparser

from server import UserRegistry

config = configparser.ConfigParser()
app = Flask(__name__)

HEADERS = {"Content-Type": "application/json"}

def index_template_data() -> dict:
    return {
        "current_user": app._server_user.name.capitalize(),
        "count_users_online": len(app._server_registry),
        "count_messages_received": 1,
        "count_messages_sent": 1,
        "get_users": sorted(get_users())
    }

@app.route("/")
def index():
    return render_template('index.html',**index_template_data())

@app.route("/users/<username>",methods=["GET"])
def get_user(username: str):
    if username:
        return app._server_registry.find_user(username)
    return "Error"

@app.route("/users/",methods=["GET"])
def get_users():
    return app._server_registry.registry

@app.route("/messages/simulate",methods=["GET"])
def messages_simulate():
    if request.args.get("user") and request.args.get("message"):
        dest = app._server_registry.find_user(request.args.get("user"))
        message = app._server_user.send_message_to(dest["username"],request.args.get("message")).decode()
        payload = app._server_user.build_payload(message=message)
        response = requests.post(f'{dest["url"]}{url_for("messages",username=app._server_user.name)}',headers=HEADERS,data=payload)
        return response.json()
    return "Error"

@app.route("/messages/<username>",methods=["GET","POST"])
def messages(username: str):
    if username:
        source = app._server_registry.find_user(username)
        if source and request.method == "GET":
            # TODO READ MESSAGE FROM <username>
            encrypted_message = app._server_user.get_message_from(source['username'])
            message = app._server_user.read_message_from(source['username']).decode()
            return {"status": HTTPStatus.OK, "message": message, "encrypted": encrypted_message, "from": source['username']}
        if source and request.method == "POST":
            # TODO RECEIVE MESSAGE FROM <username>
            app._server_user.receive_message_from(source['username'],request.json['message'])
            app.logger.info({ "message": request.json['message'], "to": app._server_user.name})
            return {"status": HTTPStatus.OK, "message": request.json['message'], "to": app._server_user.name}
    return "Error"

@app.route("/key_exchange/simulate",methods=["GET"])
def key_exchange_simulate():
    if request.method == "GET" and request.args.get("user"):
        dest = app._server_registry.find_user(request.args.get("user"))
        payload = app._server_user.build_payload(public_key=app._server_user.public_key,username=app._server_user.name)
        user_response = requests.post(f"{dest['url']}{url_for('key_exchange')}",headers=HEADERS,data=payload)
        app._server_user.exchange_key(user_response.json())
        return {"status": HTTPStatus.ACCEPTED, "info": app._server_user._exchanged_keys,"debug": user_response.json()["debug"]}

@app.route("/key_exchange",methods=["POST"])
def key_exchange():
    if request.method == "POST":
        app._server_user.exchange_key(request.json)
        response = json.loads(app._server_user.build_payload(public_key=app._server_user.public_key,username=app._server_user.name,debug=app._server_user._exchanged_keys))
        response["status"] = HTTPStatus.ACCEPTED
        return response

if __name__ == "__main__":
    config.read("server.ini")
    host_port = sys.argv[1]
    host_url = f"http://127.0.0.1:{host_port}"
    app._server_user = WebUser3SDES(sys.argv[2].lower(),url=host_url,q=int(config["DH"]["q"]))
    app._server_user.csprng = BBS
    # Register in registry
    app._server_registry = UserRegistry("registry.json")
    app._server_registry.add_user(app._server_user.name,url=host_url,username=app._server_user.name,public_key=app._server_user.public_key)
    app._server_registry.commit()
    print("> Registry updated.")
    print("> User online:")
    print(f"> {app._server_registry}")
    app.debug = True
    app.run(port=host_port)
